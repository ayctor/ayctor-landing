const mix = require('laravel-mix');
const StyleLintPlugin = require('stylelint-webpack-plugin');

mix.webpackConfig({
    module: {
        rules: [
            {
                test: /.(js)$/,
                loader: 'eslint-loader',
                enforce: 'pre',
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [
        new StyleLintPlugin({
            context: 'src/scss',
            lintDirtyModulesOnly: true,
            syntax: 'scss',
        }),
    ],
});

mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery'],
});

mix.options({
    processCssUrls: false,
});

mix.js('assets/js/app.js', 'public/js')
    .sass('assets/sass/app.scss', 'public/css')
    .sourceMaps()
    .browserSync({
        proxy: 'https://ayctor-landing.dev',
        files: ['**/*.php', '!vendor/**/*.php', 'public/**/*'],
    });
